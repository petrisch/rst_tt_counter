use std::io;

fn main() {

    const MAX_SCORE: i8 = 11;
    const WINNING_INCREMENT: i8 = 2;

    struct Standing {
       name_player_a: String,
       points_player_a: i8,
       name_player_b: String,
       points_player_b: i8
    }

    let mut standing = Standing {
        name_player_a: String::from("A"),
        points_player_a: 0,
        name_player_b: String::from("B"),
        points_player_b: 0,
    };

    println!("This is a Table Tennis Score Counter:");

loop {

    println!("Who made the point?");
    println!("A for Player A; B for Player B");

    // The variable to read from stdin
    let mut input = String::new(); 

    // Clear the screen
    print!("\x1B[2J\x1B[1;1H");

    // Make shure what you read is actually something
    io::stdin()
        .read_line(&mut input)
        .expect("Wrong input");

    println!("Player: {} made a point", input);

    standing = count_score(input, standing);

    println!("The score is: A/{} : B/{}",
        standing.points_player_a, 
        standing.points_player_b);
    
    let winner = determine_winner(&standing);

        println!("{} won a Set", winner);
}

fn determine_winner (standing: &Standing) -> String {

    if standing.points_player_a >= MAX_SCORE &&
        &standing.points_player_a - &standing.points_player_b
            >= WINNING_INCREMENT {
                String::from(&standing.name_player_a)
                }
    else if 
        standing.points_player_b >= MAX_SCORE &&
        standing.points_player_b - standing.points_player_a
            >= WINNING_INCREMENT {
                String::from(&standing.name_player_b)
                }
    else{
        String::from("none")
    }
}

fn count_score(name_player: String, mut standing: Standing) -> Standing {

    if name_player.trim().eq(&standing.name_player_a) {
        standing.points_player_a += 1;
        standing
    }
    else if name_player.trim().eq(&standing.name_player_b) {
        standing.points_player_b += 1;
        standing
    }
    else{
        println!("This is not a player");
        standing
    }
 }
}
